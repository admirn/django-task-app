# Simple Django task app

This is a simple task app. You can easily save and edit your own tasks

## Installing (works with python2.7)
```
git clone https://admirn@bitbucket.org/admirn/django-task-app.git
```
```
cd django-task-app
```
```
cd django-task-app
```
```
python manage.py makemigrations
```
```
python manage.py migrate
```
```
python manage.py runserver
```
