from __future__ import unicode_literals
from django.contrib import admin
from taskuser.models import Task

class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description', 'status', 'editor',)
    list_display_links = ('id', 'name')
    search_fields = ('name', 'id')
    list_filter = ('status', 'editor',)
    list_per_page = 10

admin.site.register(Task, TaskAdmin)
