from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

class Task(models.Model):
    name = models.CharField(max_length=160)
    description = models.TextField()
    status = models.BooleanField(default=False)
    creator = models.ForeignKey(User, null=False, related_name='creator')
    editor = models.ForeignKey(User, blank=True, null=True, related_name='editor')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('task_edit', kwargs={'pk': self.pk})
