from django import forms
from django.forms import ModelForm
from .models import Task
from django.contrib.auth.models import User

class AddTaskForm(forms.ModelForm):
    name = forms.CharField(label='Your name', max_length=160, help_text='100 characters max.')
    description = forms.CharField(label='Description ',widget=forms.Textarea)
    creator = forms.ModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = Task
        fields = ['name', 'description']

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if "some-name" not in name:
            raise forms.ValidationError('Name must be some-name.')
        return name

class EditTaskForm(forms.ModelForm):
    name = forms.CharField(label='Your name', max_length=160)
    description = forms.CharField(label='Description ',widget=forms.Textarea)

    class Meta:
        model = Task
        fields = ['name', 'description', 'status',
        # 'creator'
        ]
