from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^create_task/$', views.create_task, name='create_task'),
    url(r'^update_task/(?P<pk>\d+)$', views.update_task, name='update_task'),
    url(r'^delete_task/(?P<pk>\d+)$', views.delete_task, name='delete_task'),
    url(r'^markdone_task/(?P<pk>\d+)$', views.markdone_task, name='markdone_task'),
]
