from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from taskuser.models import Task
from .forms import AddTaskForm, EditTaskForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages

from taskuser.models import Task
from django.forms import ModelForm
from django import forms

@login_required
def index(request):
    if request.method == 'POST':
        selected_option = request.POST['task']
        if selected_option == '1':
            tasks = Task.objects.filter(status=True)
            content = {
                'tasks': tasks,
                'selected_option': selected_option
            }
            template_name = 'taskuser/index.html'

            return render(request, template_name, content)
        else:
            if selected_option == '2':
                tasks = Task.objects.filter(status=False)
                content = {
                    'tasks': tasks,
                    'selected_option': selected_option
                }
                template_name = 'taskuser/index.html'

                return render(request, template_name, content)

    tasks = Task.objects.all().order_by("-id")[:25]
    content = {
        'tasks': tasks
    }
    template_name = 'taskuser/index.html'

    return render(request, template_name, content)

@login_required
def create_task(request):
    form = AddTaskForm()
    if request.method == 'POST':
        form = AddTaskForm(request.POST or None)
        if form.is_valid():
            Task.objects.create(
                name=form.cleaned_data['name'],
                description=form.cleaned_data['description'],
                creator_id=request.user.id,
            )
            messages.add_message(request, messages.INFO, 'Your task was added.')
            return HttpResponseRedirect(reverse('create_task'))
        else:
            return render(request, 'taskuser/create_task.html', {'form': form})
    else:
        form = AddTaskForm()
    content = {
        'form': form,
    }
    template_name = 'taskuser/create_task.html'

    return render(request, template_name, content)

@login_required
def update_task(request, pk):
    task = get_object_or_404(Task, id=pk)
    if request.user.id == task.creator_id:
        form = EditTaskForm(request.POST or None, instance=task)
        if request.method == 'POST':
            form = EditTaskForm(request.POST or None, instance=task)
            if form.is_valid():
                obj = Task.objects.get(id=pk)
                obj.name = form.cleaned_data['name']
                obj.description = form.cleaned_data['description']
                obj.status = form.cleaned_data['status']
                obj.editor = request.user
                obj.save()
                messages.add_message(request, messages.INFO, 'Your task was successfully updated.')
                return HttpResponseRedirect(reverse('update_task', args=[pk]))
            else:
                return render(request, 'taskuser/update_task.html', {'form': form})
    else:
        messages.add_message(request, messages.INFO, 'You are not creator of this task.')
        return HttpResponseRedirect(reverse('index'))
    template_name = 'taskuser/update_task.html'
    return render(request, template_name, {'form':form})

@login_required
def delete_task(request, pk):
    task = get_object_or_404(Task, id=pk)
    if request.user.id == task.creator_id:
        if request.method=='POST':
            task.delete()
            messages.add_message(request, messages.INFO, 'The task was deleted successfully.')
            return HttpResponseRedirect(reverse('index'))
    else:
        messages.add_message(request, messages.INFO, 'You are not creator of this task.')
        return HttpResponseRedirect(reverse('index'))
    template_name = 'taskuser/delete_task.html'
    return render(request, template_name, {'object':task})

@login_required
def markdone_task(request, pk):
    task = get_object_or_404(Task, id=pk)
    obj = Task.objects.get(id=pk)
    if obj.status == True:
        messages.success(request, 'The task is already done.')
        return HttpResponseRedirect(reverse('index'))
    else:
        obj.status = True
        obj.editor = request.user
        obj.save()
        messages.success(request, 'The task was updated to done.')
        return HttpResponseRedirect(reverse('index'))
